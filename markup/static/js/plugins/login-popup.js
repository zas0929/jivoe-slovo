$(document).ready(function () {
    $('.login-trigger').on('click', function () {
        $('.login').fadeIn(200);
        $('.overlay').fadeIn(200);
    });

    $('.register-trigger').on('click', function () {
        $('.register').fadeIn(200);
        // $('.overlay').fadeIn(200);
    });

    $('.overlay').on('click', function () {
        $('.login').fadeOut(200);
        $('.register').fadeOut(200);
        $('.overlay').fadeOut(200);
    });
});
