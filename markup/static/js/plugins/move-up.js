$(document).on('click', '.move-up__button', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 500);
});
