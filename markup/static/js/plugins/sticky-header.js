$(document).ready(function () {
    const $header = $('#header');
    let position = $(window).scrollTop();
    const $window = $(window);

    function stickHeader() {
        const scroll = $(window).scrollTop();

        if (scroll > position) {
            $header.removeClass('header--sticky');
            $header.addClass('header--sticky-bottom');

        } else {
            $header.removeClass('header--sticky-bottom');
            $header.addClass('header--sticky');
        }
        position = scroll;
    }

    stickHeader();

    $window.on('scroll', function () {
        stickHeader();
    });
});
