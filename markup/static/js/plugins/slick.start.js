$(document).ready(function () {
    $('.kurs__slider').slick({
        dots: true
    });

    $('.reviews-slider__slider').slick({
        dots: true,
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 769,
                settings: 'unslick'
            },
        ]
    });

    $('.articles-slider').slick({
        dots: false,
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('.customers__slider').slick({
        dots: true,
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });
});
